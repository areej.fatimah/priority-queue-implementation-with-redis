package com.example.demo.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Receiver {
	String QUEUE="message-queue";
	
	@Autowired
	private RedisTemplate<String,String> template;
	
	
	@GetMapping("/consume")
	public String publish(){
			template.opsForZSet().popMin(QUEUE);
			return template.opsForZSet().zCard(QUEUE).toString()+" messages are left";
		
	}
}
