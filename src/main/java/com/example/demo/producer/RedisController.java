package com.example.demo.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {
	
	@Autowired
	private RedisTemplate<String,String> template;
	
	String QUEUE="message-queue";
	
	@PostMapping("/publish/{value}/{score}")
	public String publish(@PathVariable("value") String value,@PathVariable("score") double score) {
		template.opsForZSet().add(QUEUE, value.toString(), score);
		return "Successfully added";
	}
	
	
}
