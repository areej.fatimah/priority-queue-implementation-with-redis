package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;

@Configuration
public class Config {

	
	@Bean
	public JedisConnectionFactory connectionFactory() {
		RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
		configuration.setHostName("localhost");
		configuration.setPort(6379);
		JedisConnectionFactory jedisConnectionfactory =new JedisConnectionFactory(configuration);
		jedisConnectionfactory.afterPropertiesSet();
		return jedisConnectionfactory;
		  }
	@Bean
	public RedisTemplate<String,String> template(){
		RedisTemplate<String,String> template= new StringRedisTemplate();
		template.setConnectionFactory(connectionFactory());
		template.setValueSerializer(new GenericToStringSerializer<String>(String.class));
		return template;
	}
	
}
